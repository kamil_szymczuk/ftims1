# Grzybobranie #
Które grzyby zjadać samemu, a którymi częstować anegdotyczną teściową.

### Cel Analizy ###
Celem analizy jest odpowiedzenie na pytanie czy można określić (i z jakim prawdopodobieństwem) czy dany grzyb jest trujący czy jadalny oraz jakimi fizycznymi parametrami powinno się sugerować sugerować.

### Opis danych ###
Dane zostały zebrane na podstawie książki "The Audubon Society Field Guide to North American Mushrooms".
Składają się z parametrów określających:

* **kształt kapelusza**: dzwon, stożek, wypukły, płaski, kulisty, wklęsły
* **powierzchnie kapelusza**: łykowata, rowkowata, łuskowata, gładka 
* **kolor kapelusza**: brązowy, płowy, cynamonowy, szary, zielony, różowy, fioletowy, czerwony, biały, żółty 
* **zasinienia**: występują, nie występują 
* **zapach**: migdałowy, anyżowy, kreozotowy, rybny, odrzucający/nieprzyjemny, stęchły, brak, intensywny, ostry 
* **położenie blaszek**: przyległe, malejące, niezwiązane, karbowane 
* **gęstość blaszek**: bliskie, gęste, odległe 
* **rozmiar blaszek**: szerokie, wąskie 
* **kolor blaszek**: czarne, brązowe, płowe, czekoladowe, szare, zielone, pomarańczowe, różowe, fioletowe,czerwone, białe, żółte 
* **kształt korzenia**: rozszerzający się, zwężający się 
* **nasada korzenia**: bulwiasta, maczugowata, kubkowata, stała, ryzomorficzna, zakorzeniona, brak
* **powierzchnia korzenia powyżej pierścienia**: łykowaty, łuskowaty, delikatny, gładki 
* **powierzchnia korzenia poniżej pierścienia**: łykowaty, łuskowaty, delikatny, gładki 
* **kolor korzenia powyżej pierścienia**: brązowy, płowy, cynamonowy, szary, pomarańczowy, różowy, czerwony, biały, żółty 
* **kolor korzenia poniżej pierścienia**: brązowy, płowy, cynamonowy, szary, pomarańczowy, różowy, czerwony, biały, żółty 
* **błona**: częściowa, całościowa 
* **kolor błony**: brązowy, pomarańczowy, biały, żółty 
* **liczba pierścieni**: brak, jeden, dwa 
* **typ pierścienia**: pajęczynowata, zanikająca, kołnierz, duży, brak, wiszący, osłonka, strefa 
* **kolor zarodników**: czarny, brązowy, płowy, czekoladowy, zielony, pomarańczowy, fioletowy, biały, żółty 
* **populacja**: gęsta, skupiona, liczna, rozproszona, żadka, samotna 
* **siedlisko**: trawy, liście, łąki, drogi, miasta, odpady, lasy

Parametr **class** opisuje czy dany grzyb jest jadalny czy trujący. Jest to nasza zmienna celu.

### Przygotowanie danych ###
Dane w badanym zbiorze nie wymagają wielu zmian. Zmieniono jedynie wartości parametrów na pełne wyrazy (zmiana raczej kosmetyczna).
Usunięto także zmienną **błona** ponieważ przyjmowała wyłącznie jedną wartość.

### Analiza danych ###
Zbiór danych został podzielony na dwa zbiory, uczący i testujący, w stosunku 1:3.
Zbudowano drzewo decyzyjne, las losowy oraz zbadany istotność parametrów.

### Wnioski ###
Otrzymany za pomocą lasu losowego model pozwala określić na podstawie podanych parametrów czy grzyb jest jadalny czy trójący z prawdopodobieństwem bliskim 100%.
Najistotniejszymi zmiennumi okazały się **zapach**, **kolor zarodników**, **populacji**, **środowiska**.
Ograniczenie modelu wyłącznie do tych zmiennych nie zmniejszyło znacznie wyników. 